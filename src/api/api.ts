const publicApi = 'https://jsonplaceholder.typicode.com';
const postsApi = `${publicApi}/posts`;
const postsPerPage = 10;

const myFetch = async (url: string) => {
	return fetch(url)
		.then(response => {
			if (response.status === 404) {
				throw new Error(response.statusText);
			}
			return response.json();
		});
};

export const api = {
	posts: {
		fetchPostsByPage: (page: number) => {
			const params = new URLSearchParams({ _page: page.toString(), _per_page: postsPerPage.toString() }); // _per_page (default = 10)
			return myFetch(`${postsApi}?${params}`);
		},
		fetchPosts: (page: number) => {
			const params = new URLSearchParams({ _start: '0', _end: (page * postsPerPage).toString() }); // _end === page * perPage
			return myFetch(`${postsApi}?${params}`);
		},
		fetchPostDetails: (id: string) => myFetch(`${postsApi}/${id}`), // get post by id
	},
};