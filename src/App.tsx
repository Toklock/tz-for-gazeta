import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Header } from './components/layouts/Header/Header';
import { PostPage } from './components/pages/PostPage/PostPage';
import { MainPage } from './components/pages/MainPage/MainPage';
import { PostsPage } from './components/pages/PostsPage/PostsPage';
import './styles/index.scss';

function App() {
	return (
		<>
			<Header />
			<main>
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="/posts" element={<PostsPage />} />
					<Route path="/posts/:id" element={<PostPage />} />
				</Routes>
			</main>
		</>
	);
}

export default App;
