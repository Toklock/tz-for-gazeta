import styles from './Header.module.scss';
import { NavLink } from 'react-router-dom';

export const Header = () => {
	return (
		<header className={styles.header}>
			<div className={`${styles.header__container} container`}>
				<NavLink to={'/'} className={styles.header__button}>
					main page
				</NavLink>
				<NavLink to={'/posts'} end className={styles.header__button}>
					posts
				</NavLink>
			</div>
		</header>
	);
};