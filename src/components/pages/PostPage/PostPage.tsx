import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { api } from '../../../api/api';
import { IPost } from '../../../models/posts';
import styles from './PostPage.module.scss';

export const PostPage = () => {

	const { id } = useParams();
	const [post, setPost] = useState<IPost>();
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		if (!id) return;

		setIsLoading(true);
		api.posts.fetchPostDetails(id)
			.then(setPost)
			.finally(() => setIsLoading(false));
	}, [id]);

	if (isLoading) return <span>Loading...</span>;
	if (!post) return <span>Error!</span>;

	return (
		<div className={styles.post}>
			<section>
				<div className="container">
					<h2 className={styles.post__name}>{post.title}</h2>
					<p className={styles.post__description}>{post.body}</p>
				</div>
			</section>
		</div>
	);
};