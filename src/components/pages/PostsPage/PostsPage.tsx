import React, { useEffect, useRef, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import { Post } from './Post/Post';
import { api } from '../../../api/api';
import { IPost } from '../../../models/posts';
import styles from './PostsPage.module.scss';

const maxPageNumberWithoutLoadButton = 6;
const totalPages = 10;

export const PostsPage = () => {
	const [searchParams, setSearchParams] = useSearchParams();
	const refForLoadingMorePosts = useRef<HTMLDivElement>(null);
	const [posts, setPosts] = useState<IPost[]>();
	const [page, setPage] = useState(1);

	useEffect(() => {
		const pageFromParams = Number(searchParams.get('page')); // get page from params

		if (pageFromParams) { // check page from params
			api.posts.fetchPosts(pageFromParams)
				.then((posts) => {
					setPosts(posts);
					setPage(pageFromParams);
				});
		} else {
			getPosts(page); // get first 10 posts
		}
	}, []);

	const getPosts = (page: number) => {
		api.posts.fetchPostsByPage(page)  // get 10 posts
			.then((posts) => {
				setPage(page);
				setPosts(prevPosts => [...(prevPosts || []), ...posts]);
				setSearchParams({ page: page.toString() }); // add page number into params
			});
	};

	const postsItems = posts?.map((post) => <Post key={post.id} post={post} />);

	useEffect(() => {
		if (!refForLoadingMorePosts.current) return;
		if (page >= maxPageNumberWithoutLoadButton) return; // use observer only if page <= 6
		if (!posts) return;

		const observer = new IntersectionObserver(([entry]) => {
			if (entry.isIntersecting) {
				getPosts(page + 1);
			}
		}, { rootMargin: '300px' }); // root margin for best experience

		observer.observe(refForLoadingMorePosts.current);

		return () => observer.disconnect(); // clear observer
	}, [posts]);

	return (
		<section className={styles.posts}>
			<div className={`${styles.posts__container} container`}>
				<h1 className={styles.posts__title}>Posts</h1>

				{posts
					? (
						<ul className={styles.posts__list}>
							{postsItems}
						</ul>
					)
					: <div>Loading...</div>
				}

				{page >= maxPageNumberWithoutLoadButton && page < totalPages // show load button or elem for observer
					? <button onClick={() => getPosts(page + 1)} className={styles.posts__moreButton}>загрузить еще</button>
					: <div ref={refForLoadingMorePosts} />
				}
			</div>
		</section>
	);
};