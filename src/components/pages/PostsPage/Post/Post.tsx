import React from 'react';
import { Link } from 'react-router-dom';
import { IPost } from '../../../../models/posts';
import styles from './Post.module.scss';

interface IProps {
	post: IPost;
}

export const Post = ({ post }: IProps) => {
	return (
		<li className={styles.post}>
			<Link className={styles.post__link} to={`/posts/${post.id}`}>
				<div className={styles.post__number}>post #{post.id}</div>
				<h2 className={styles.post__title}>{post.title}</h2>
				<p className={styles.post__paragraph}>{post.body}</p>
			</Link>
		</li>
	);
};