# Тестовое задание для ФГБУ Редакция Российской газеты

### Технологии

-  [React](https://ru.react.js.org/docs/getting-started.html)
-  [TypeScript](https://www.typescriptlang.org/docs/)
-  [SCSS](https://sass-scss.ru/documentation/)
-  [HTML](https://htmlbook.ru/html)

### Разработка

#### NodeJS

Для установки и запуска проекта, необходим [NodeJS](https://nodejs.org/) v20.10.0

#### Установка зависимостей

Для установки зависимостей, выполните команду:

```sh
npm i
```

#### Запуск Development сервера

Чтобы запустить сервер для разработки, выполните команду:

```sh
npm start
```

#### Создание билда

Чтобы выполнить production сборку, выполните команду:

```sh
npm run build
```
